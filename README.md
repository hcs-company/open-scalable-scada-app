## Introduction

The SCADA (Supervisory Control and Data Acquisition) landscape is on the brink of a revolutionary transformation. Leading industry players, including Energinet, Prorail, and HCS Company, are championing the shift towards a fully open-sourced SCADA model. With this bold move they would like to redefine the standards of scalability, maintainability, and flexibility in industrial control systems. Join us if you like! 

## Background
The contributors to this document are currently involved in the development and deployment of SCADA systems, and have indirectly observed a way how modern development, operation, and scaling of countless industrial control applications can be realized through our work with leading organizations such as Energinet, Prorail, HCS Company and many more to come!

This document expresses our observations on a wide variety of SCADA systems in diverse industrial environments. It presents ideal practices for SCADA system development, with particular attention to the dynamics of organic growth over time, the collaboration between engineers working on the system’s codebase, and strategies to avoid long term lock-in.

Our motivation is to raise awareness of some systemic problems we’ve observed in traditional SCADA system development, to provide a shared vocabulary for discussing these issues, and to offer a set of broad conceptual solutions accompanied by relevant terminology. 

# Open Scalable SCADA
## I. Codebase
**One open source codebase tracked in revision control, many deploys**

A Open Scalable SCADA application is always tracked in a version control system, such as Git, and has a single codebase that can be deployed in multiple environments.

## II. Dependencies
**Explicitly declare and isolate dependencies**

Open Scalable SCADA applications explicitly declare all dependencies and isolate them to ensure consistency across different environments.

## III. Config

**Store configuration in the environment**

Configuration that varies between deployments (e.g., connection strings, credentials) is stored in the environment.


## IV. Backing Services

**Treat backing services as attached resources**

Backing services (e.g., databases, messaging systems) are treated as attached resources and can be swapped without changing the code.


## V. Build, Release, Run

**Strictly separate build and run stages**

The build stage converts the code into an executable bundle, the release stage combines the build with configuration, and the run stage executes the application.



## VI. Processes

**Execute the app as one or more stateless processes**

Open Scalable SCADA applications run as stateless processes with state stored in backing services.



## VII. Port Binding

**Export services via port binding**

Open Scalable SCADA applications export services by binding to a port and listening for requests.



## VIII. Concurrency

**Scale out via the process model**

Open Scalable SCADA applications achieve scale by running multiple processes, which can be distributed across multiple machines.



## IX. Disposability

**Maximize robustness with fast startup and graceful shutdown**

Open Scalable SCADA applications are designed to start up and shut down quickly, allowing for rapid scaling and recovery.



## X. Dev/Prod Parity

**Keep development, staging, and production as similar as possible**

Minimize differences between development, staging, and production environments to avoid surprises during deployment.



## XI. Logs

**Treat logs as event streams**

Logs are treated as event streams and can be routed to various destinations for analysis and long-term storage.



## XII. Admin Processes

**Run admin/management tasks as one-off processes**

Administrative tasks (e.g., database migrations) are run as one-off processes in the same environment as the application.



## Conclusion

Adopting the Open Scalable SCADA methodology ensures that SCADA applications are scalable, maintainable, and resilient. By following these guidelines, developers can build robust SCADA systems that meet the demands of modern industrial environments.


# Who's joing the initiative?

## Energinet
Denmark's state-owned enterprise responsible for electricity and gas transmission systems, is spearheading the effort to enhance transparency and interoperability in SCADA systems. By embracing an open-source approach, Energinet aims to foster innovation and collaboration within the energy sector, ensuring a resilient and sustainable energy infrastructure.

## Prorail
The Dutch railway infrastructure management organization, is committed to modernizing railway operations through open-source SCADA solutions. By leveraging open standards and community-driven development, Prorail is set to achieve higher efficiency, better safety measures, and seamless integration across various systems, thereby setting a new benchmark for railway infrastructure management worldwide.

## HCS Company
A leader in advanced software defined open source platform solutions, recognizes the potential of open-source SCADA in driving technological advancements and operational excellence. HCS Company is dedicated to creating a more adaptable and secure industrial environment by promoting open-source principles, which will enable rapid development, customization, and deployment of SCADA applications.

Together, these more or less brave pioneers are poised to transform the SCADA ecosystem. By advocating for a fully open-sourced model, Energinet, Prorail, and HCS Company are not only trying to push the boundaries of technological innovation but also paving the way for a future where SCADA systems are more robust, scalable, and accessible to all.

# Target audience
Any engineer developing SCADA applications which operate as a service, as well as operations engineers who deploy or manage these SCADA systems. SCADA Architects and project managers are also encouraged to join us. 

